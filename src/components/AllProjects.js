/**
 * Created by user on 10.01.2017.
 */
import React, { Component } from 'react';
import '../css/AllProjects.css';
import { Link } from 'react-router';
import cookie from 'react-cookie';
import $ from 'jquery';
import {Grid, Tabs, Tab, Table, ButtonToolbar, DropdownButton, MenuItem} from 'react-bootstrap/lib';

import { browserHistory } from 'react-router';

//allProjects
const DETAIL_TEAM = 'https://coffeecubes.ru/api/v1/teams/detail/?token='+cookie.load('token');
const CREATE_PROJECT = 'https://coffeecubes.ru/api/v1/projects/create/?token='+cookie.load('token');
const DELETE_PROJECT = 'https://coffeecubes.ru/api/v1/projects/delete/?token='+cookie.load('token');
const UPDATE_PROJECT = 'https://coffeecubes.ru/api/v1/projects/update/?token='+cookie.load('token');
const PROJECT_LIST = 'https://coffeecubes.ru/api/v1/projects/list_team/?token='+cookie.load('token');

class User {

}

class RequestToApi {
  constructor(url, data) {
    this.url = url;
    this.data = data;
  }
  request() {
    fetch(this.url, {
      method: 'POST',
      mode: 'cors',
      body:JSON.stringify(this.data)
    })
    .then(function(response) {
      return response.json();
    })
    .then(function(json) {
      console.log(json);
      let result = json;
      return result;
    })
    .catch( console.log );
  }
}

class AllProjects extends Component {
  constructor(props){
    super(props);
    this.componentDidMount = this.componentDidMount.bind(this);
    this.handleCreateProject = this.handleCreateProject.bind(this);
    this.handleDeleteProject = this.handleDeleteProject.bind(this);
    this.handleDuplicateProject = this.handleDuplicateProject.bind(this);

    this.state = {
      open: false,
      teamName : null,
      teamId : null,
      owner: null,
      members : null,
      projects : null
    };
  }

  componentDidMount = ()=>{
    //Получаем данные о команде
    let _this = this;
    let id = this.props.params.team_id; // Получаем данные о текущем переходе по идентификаору команды
    id = parseInt(id, 10);
    let data = {"id" : id};

    fetch(DETAIL_TEAM, {
      method: 'POST',
      mode: 'cors',
      body:JSON.stringify(data)
    })
    .then(function(response) {
      return response.json();
    })
    .then(function(result) {
      _this.setState({teamName : result.name});
      _this.setState({teamId : result.id});
      var owner = new User(result.owner);
      _this.setState({ownerEmail : result.owner.email});
      _this.setState({ownerName : result.owner.name});
      _this.setState({owner : owner});
      _this.setState({members : result.members});
      //console.log(result);
    })
    .catch( console.log );

    //Получаем список проектов
    let projectData = {"team_id" : id};
    fetch(PROJECT_LIST, {
      method: 'POST',
      mode: 'cors',
      body:JSON.stringify(projectData)
    })
    .then(function(response) {
      return response.json();
     })
      .then(function(result) {
        _this.setState({projects : result.projects});
       // console.log(result);
    })
      .catch( console.log );
  };

  handleCreateProject(event){
    let id = this.props.params.team_id; // Получаем данные о текущем переходе по идентификаору команды
    id = parseInt(id, 10);
    let data = {"team_id" : id, "name" : ""};
    let _this = this;

    fetch(CREATE_PROJECT, {
      method: 'POST',
      mode: 'cors',
      body:JSON.stringify(data)
    })
    .then(function(response) {
      return response.json();
    })
    .then(function(result) {
      console.log(result);
      browserHistory.push('/project/'+result.team_id+'/'+result.id);
    })
    .catch( console.log );

    event.preventDefault();
  };

  handleDuplicateProject(event){
    let _target = event.target;
    let id = this.props.params.team_id; // Получаем данные о текущем переходе по идентификаору команды
    id = parseInt(id, 10);
    let data = {"team_id" : id, "name" : ""};

    fetch(CREATE_PROJECT, {
      method: 'POST',
      mode: 'cors',
      body:JSON.stringify(data)
    })
    .then(function(response) {
      return response.json();
    })
    .then(function(result) {
      console.log(result.id);
      console.log(_target.getAttribute('data-id'));
      let projectId = parseInt(_target.getAttribute('data-id'), 10);
      let projectName = parseInt(_target.getAttribute('data-name'), 10);
      let projectStart = parseInt(_target.getAttribute('data-start'), 10);
      let projectComment = parseInt(_target.getAttribute('data-comment'), 10);

      let data = {"id" : projectId, "name" : projectName, "start_date" : projectStart, "comment" : projectComment};
      let RequestUpdateProject = new RequestToApi(UPDATE_PROJECT, data);
      RequestUpdateProject.request();
    })
    .catch( console.log );
  }

  handleDeleteProject(event){
    console.log(event.target.getAttribute('data-id'));
    let projectId = parseInt(event.target.getAttribute('data-id'), 10);
    let data = {"project_id" : projectId};

    let RequestUpdateTask = new RequestToApi(DELETE_PROJECT, data);
    RequestUpdateTask.request();
  }

  render() {
    return (
      <Grid className="projects-all-component">
        <HeaderProjects
          handleCreateProject={this.handleCreateProject}
        />
        <Projects
          projectList={this.state.projects}
          teamId={this.props.params.team_id}
          handleDeleteProject={this.handleDeleteProject}
          handleDuplicateProject={this.handleDuplicateProject}
        />
      </Grid>
    );
  }
}

function HeaderProjects(props){
  return(
    <div className="projects-header">
      <div className="project-search-container">
        <input type="text" className="input_search-project" placeholder="Search project ..."/>
      </div>
      <div className="project-button-container">
        <input type="submit" className="input_head-project-btn" value="import" />
        <input type="submit" className="input_head-project-btn" value="export" />
        <input type="submit" className="input_head-project-btn" value="New Project" onClick={props.handleCreateProject}/>
      </div>
    </div>
  )
}

function Projects(props){
 if(props.projectList) {
   return (
     <div className="projects-container">
       <Tabs defaultActiveKey={1} id="uncontrolled-tab-example">
         <Tab eventKey={1} title="Active">
         <Table responsive>
           <thead>
             <tr>
               <th>Project</th>
               <th>Team</th>
               <th>Start<br/>date</th>
               <th>Hours<br/>budget</th>
               <th> </th>
               <th>Hours<br/>fact</th>
               <th>Finish<br/>date</th>
               <th>Status</th>
               <th>Manager</th>
               <th>Budget</th>
               <th>Net<br/>income</th>
               <th>Rentbility</th>
               <th>Actions</th>
             </tr>
           </thead>
           <tbody>
             {props.projectList.map((item, i) =>
               <tr key={i}>
                 <td>{item.name}</td>
                 <td></td>
                 <td>{item.start_date}</td>
                 <td></td>
                 <td></td>
                 <td></td>
                 <td>{item.finish_date}</td>
                 <td></td>
                 <td></td>
                 <td>{item.budget}</td>
                 <td></td>
                 <td></td>
                 <td>
                   <ButtonToolbar>
                    <DropdownButton bsStyle="default" title="Actions" noCaret id="dropdown-no-caret">
                      <Link className="link-dropdown" to={'/project/'+props.teamId+'/'+item.id}>Edit</Link>
                      <span className="link-dropdown" data-id={item.id} data-name={item.name} data-start={item.start_date} data-comment={item.comment} onClick={props.handleDuplicateProject}>Duplicate</span>
                      <span className="link-dropdown" data-id={item.id} onClick={props.handleDeleteProject}>Delete</span>
                      <MenuItem divider />
                      <span className="link-dropdown" data-id={item.id}>Close</span>
                      <span className="link-dropdown" data-id={item.id}>Pause</span>

                    </DropdownButton>
                  </ButtonToolbar>
                 </td>
               </tr>
             )}
           </tbody>
         </Table>
         </Tab>
         <Tab eventKey={2} title="Closed">
           <Table responsive>
             <thead>
               <tr>
                 <th>Project</th>
                 <th>Team</th>
                 <th>Start<br/>date</th>
                 <th>Hours<br/>budget</th>
                 <th> </th>
                 <th>Hours<br/>fact</th>
                 <th>Finish<br/>date</th>
                 <th>Status</th>
                 <th>Manager</th>
                 <th>Budget</th>
                 <th>Net<br/>income</th>
                 <th>Rentbility</th>
                 <th>Actions</th>
               </tr>
             </thead>
             <tbody>
               {props.projectList.map((item, i) =>
                 <tr key={i}>
                   <td>{item.name}</td>
                   <td></td>
                   <td>{item.start_date}</td>
                   <td></td>
                   <td></td>
                   <td></td>
                   <td>{item.finish_date}</td>
                   <td></td>
                   <td></td>
                   <td>{item.budget}</td>
                   <td></td>
                   <td></td>
                   <td>
                     <ButtonToolbar>
                      <DropdownButton bsStyle="default" title="Actions" noCaret id="dropdown-no-caret">
                        <Link className="link-dropdown" to={'/project/'+props.teamId+'/'+item.id}>Edit</Link>
                        <span className="link-dropdown" data-id={item.id}>Duplicate</span>
                        <span className="link-dropdown" data-id={item.id} onClick={props.handleDeleteProject}>Delete</span>
                        <MenuItem divider />
                        <span className="link-dropdown" data-id={item.id}>Close</span>
                        <span className="link-dropdown" data-id={item.id}>Pause</span>

                      </DropdownButton>
                    </ButtonToolbar>
                   </td>
                 </tr>
               )}
             </tbody>
           </Table>
         </Tab>
         <Tab eventKey={3} title="Paused">
           <Table responsive>
             <thead>
               <tr>
                 <th>Project</th>
                 <th>Team</th>
                 <th>Start<br/>date</th>
                 <th>Hours<br/>budget</th>
                 <th> </th>
                 <th>Hours<br/>fact</th>
                 <th>Finish<br/>date</th>
                 <th>Status</th>
                 <th>Manager</th>
                 <th>Budget</th>
                 <th>Net<br/>income</th>
                 <th>Rentbility</th>
                 <th>Actions</th>
               </tr>
             </thead>
             <tbody>
               {props.projectList.map((item, i) =>
                 <tr key={i}>
                   <td>{item.name}</td>
                   <td></td>
                   <td>{item.start_date}</td>
                   <td></td>
                   <td></td>
                   <td></td>
                   <td>{item.finish_date}</td>
                   <td></td>
                   <td></td>
                   <td>{item.budget}</td>
                   <td></td>
                   <td></td>
                   <td>
                     <ButtonToolbar>
                      <DropdownButton bsStyle="default" title="Actions" noCaret id="dropdown-no-caret">
                        <Link className="link-dropdown" to={'/project/'+props.teamId+'/'+item.id}>Edit</Link>
                        <span className="link-dropdown" data-id={item.id}>Duplicate</span>
                        <span className="link-dropdown" data-id={item.id} onClick={props.handleDeleteProject}>Delete</span>
                        <MenuItem divider />
                        <span className="link-dropdown" data-id={item.id}>Close</span>
                        <span className="link-dropdown" data-id={item.id}>Pause</span>

                      </DropdownButton>
                    </ButtonToolbar>
                   </td>
                 </tr>
               )}
             </tbody>
           </Table>
         </Tab>
       </Tabs>
     </div>
   )
 }else{
   return(<span/>)
 }
}

export default AllProjects;
