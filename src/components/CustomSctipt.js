/**
 * Created by user on 20.02.2017.
 */

export default class RequestToApi {
  constructor(url, data) {
    this.url = url;
    this.data = data;
  }
  get request() {
    fetch(this.url, {
      method: 'POST',
      mode: 'cors',
      body:JSON.stringify(this.data)
    })
    .then(function(response) {
      // console.log(response.headers.get('Content-Type'));
      // console.log(response.status);

      return response.json();
    })
    .then(function(json) {
      console.log(json);
      let result = json;
      return result;
    })
    .catch( console.log );
  }
}