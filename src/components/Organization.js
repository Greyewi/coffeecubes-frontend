/**
 * Created by user on 24.01.2017.
 */
import React, { Component } from 'react';
import '../css/Profile.css';
import { Link } from 'react-router';
import cookie from 'react-cookie';
import $ from 'jquery';
import Accordion from 'react-bootstrap/lib/Accordion';
import Panel from 'react-bootstrap/lib/Panel';
import {Grid, Row, Col} from 'react-bootstrap/lib';


const CREATE_ORGANIZATION = 'https://coffeecubes.ru/api/v1/organizations/create/?token='+cookie.load("token");
const RETURN_ORGANIZATION = 'https://coffeecubes.ru/api/v1/organizations/list/?token='+cookie.load("token");
const UPDATE_ORGANIZATION = 'https://coffeecubes.ru/api/v1/organizations/update/?token='+cookie.load("token");

class Organization extends Component {
  constructor(props) {
    super(props);
    this.handleCreateClick = this.handleCreateClick.bind(this);
    this.handleUpdateChange = this.handleUpdateChange.bind(this);
    this.componentDidMount = this.componentDidMount.bind(this);
    this.state = {result: null, ownerEmail: null, ownerName: null};
  }

  componentDidMount = ()=>{
    $.ajax({
      type: "POST",
      url: RETURN_ORGANIZATION,
      xhrFields: {
        withCredentials: false
      },
      data : JSON.stringify({}),
      crossDomain: true,
      dataType: 'json',
      success: (result)=>{
        this.setState({result : result});
      },
      error: function(xhr, status, err) {
        console.error(RETURN_ORGANIZATION, status, err.toString());
      }
    });
  };

  handleCreateClick(event){
    let name = $('#create-organization').val();
    let data = {'name' : name};
    $.ajax({
      type: 'POST',
      url: CREATE_ORGANIZATION,
      data:JSON.stringify(data),
      processData: false,
      crossDomain: true,
      success: function(result) {
        console.log('организация создалась');
      },
      error: function(xhr, status, err) {
        console.error(CREATE_ORGANIZATION, status, err.toString());
      }
    });
    event.preventDefault();
  };

  handleUpdateChange(event){
    if((event.target.value !== "") && (event.target.value !== event.target.placeholder)){ // в placeholder храним изначальное имя
      //console.log(event.target.placeholder +'  '+event.target.value);
      let id = parseInt(event.target.parentNode.getElementsByTagName('span')[0].innerHTML, 10);
      let name = event.target.value;
      let data = {"name" : name, "id" : id };
      $.ajax({
        type: 'POST',
        url: UPDATE_ORGANIZATION,
        xhrFields: {
          withCredentials: false
        },
        data: JSON.stringify(data),
        contentType: false,
        processData: false,
        crossDomain: true,
        headers: {
        },
        success: function(result) {
          console.log('Организация переименовалась');
        },
        error: function(xhr, status, err) {
          console.error(UPDATE_ORGANIZATION, status, err.toString());
        }
      });
    }
  };

  renderData(){
    if(this.state.result !== null){
      let _this = this;
      return(
        <div className="left-form">
          <h3>Organizations</h3>
          <Accordion>
            {_this.state.result.organizations.map( (item, i) =>
              <Panel header={item.name} key={i} eventKey={i}>
                <h4 className="team-title">
                  Organization:
                  <input placeholder={item.name} value={this.state.tempName} className="update-team-input" onFocus={this.autoPassName} onBlur={this.handleUpdateChange} onKeyUp={this.handleUpdateKeyUp} />
                  <div className="temp-team-name" >{item.name}</div>
                  <span className="hidden">{item.id}</span>
                </h4>
                <p>Owner: {item.owner.name}<span className="opa-grey"> {item.owner.email} </span></p>
                <Link className="btn main-btn" to={'organization/teams/' + item.id}>Detail</Link>
              </Panel>
            )}
          </Accordion>
        </div>
      );
    }
  }

  handleUpdateKeyUp(event){
    let str = event.target.value;
    event.target.parentNode.parentNode.parentNode.parentNode.getElementsByTagName("a")[0].innerHTML = str;
    if(event.key === "backspace"){
      event.target.parentNode.parentNode.parentNode.parentNode.getElementsByTagName("a")[0].innerHTML.substring(0, str.length - 1)
    }
  }

  autoPassName(event){
    let text = event.target.placeholder;
    event.target.value = text;
  }

  render() {
    return (
      <Grid className="valid-component">
        <h2>Organization</h2>
        <Row className="show-grid">
          <Col xs={12} md={6} >
            <div id="ListUl" className="organization-list">{this.renderData()}</div>
          </Col>
          <Col xs={12} md={6} >
            <CreateOrganizationRender handleCreateClick={this.handleCreateClick} />
          </Col>
        </Row>
      </Grid>
    );
  }
}


function CreateOrganizationRender(props){
  return(
    <div className="organization-container">

      <aside className="right-form">
        <h3 className="create-organization-title">Create Organization</h3>
          <form id="up-form" onSubmit={props.handleCreateClick} >
            <input className="btn form-input" id="create-organization" type="text" placeholder="Organization name" />
            <input className="btn main-btn" type="submit" value="Create Organization"/>
         </form>
        <Link to="/">Back</Link>
      </aside>
    </div>
  )
}


export default Organization;