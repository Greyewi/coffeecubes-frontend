/**
 * Created by user on 13.12.2016.
 */
import React, { Component } from 'react';
import logo from '../logo.svg';
import '../css/Header.css';
import cookie from 'react-cookie';
import { browserHistory } from 'react-router';


const JSONToken = '{"token": "'+cookie.load('token')+'"}';
console.log('from Header '+JSONToken);

function getCookies(){
    if(!cookie.load('token')){
      return true;
    } else{
      return false;
    }
  }

// var _results = window.location.href.split('/');
// const token = '{"token": "'+_results[4]+'"}';

class Header extends Component {
  render() {
    if(getCookies() === true) {
      var path = window.location.href.split('/');
      if (
        (path[3] !== 'restore_password') &&
        (path[3] !== 'confirm_email') &&
        (path[3] !== 'confirm_invite')) {
        browserHistory.push('/')
      }
        return (
        <div/>
      );
    } else{
      return (
        <div className="header-container">
          <div className="App-header">

            <div className="link-container">
              <img src={logo} className="App-logo" alt="logo" />
              <a href="/profile">Profile</a>
            </div>
            <div className="link-container">
              <img src={logo} className="App-logo" alt="logo" />
              <a href="#">Dashboard</a></div>
            <div className="link-container">
              <img src={logo} className="App-logo" alt="logo" />
              <a href="/organization">My Cabinet</a></div>
            <div className="link-container">
              <img src={logo} className="App-logo" alt="logo" />
              <a href="#">Teams</a></div>
            <div className="link-container">
              <img src={logo} className="App-logo" alt="logo" />
              <a href="#">Projects</a></div>
            <div className="link-container">
              <img src={logo} className="App-logo" alt="logo" />
              <a href="#">Finance</a></div>
            <div className="link-container">
              <img src={logo} className="App-logo" alt="logo" />
              <a href="#">Reports</a></div>
            <div className="link-container">
              <img src={logo} className="App-logo" alt="logo" />
              <a href="#">Integrations</a></div>
            <div className="link-container">
              <img src={logo} className="App-logo" alt="logo" />
              <a href="#">Settings</a></div>
            <div className="link-container">
              <img src={logo} className="App-logo" alt="logo" />
              <a href="#">Logout</a></div>
          </div>
        </div>
      );
    }


  }
}

export default Header;
