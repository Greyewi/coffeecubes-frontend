import React, { Component } from 'react';
import '../css/SignUp.css';
import { Link } from 'react-router';
import $ from 'jquery';
import cookie from 'react-cookie';
import Grid from 'react-bootstrap/lib/Grid';
import Row from 'react-bootstrap/lib/Row';
import Col from 'react-bootstrap/lib/Col';

const RegisterAPI = 'https://coffeecubes.ru/api/v1/login/register/';
const LoginAPI = 'https://coffeecubes.ru/api/v1/login/';

class LoginControl extends React.Component {
    constructor(props) {
      super(props);
      this.handleLogoutClick = this.handleLogoutClick.bind(this);
      this.handleSubmit = this.handleSubmit.bind(this);
      this.handleSignIn = this.handleSignIn.bind(this);
      this.handleMailChange = this.handleMailChange.bind(this);
      this.handleNameChange = this.handleNameChange.bind(this);
      this.handlePasswordChange = this.handlePasswordChange.bind(this);
      this.handlePasswordRepeatChange = this.handlePasswordRepeatChange.bind(this);

      this.state = {token: cookie.load('token')};
    }

    handleMailChange(){
      let email = $('#sign-up-email').val();
      if (email === '' || /^([a-z0-9_\.-])+@[a-z0-9-]+\.([a-z]{2,4}\.)?[a-z]{2,4}$/i.test(email) === false){
        $(".err-alert").remove();
        $('#up-form').append('<div class="err-alert"><div>Incorrect email</div>');
        setTimeout(function() { $( ".err-alert" ).remove(); }, 5000);
      }
    }

    handleNameChange(){
      let name = $('#sign-up-name').val();
      if (name === ''){
        $(".err-alert").remove();
        $('#up-form').append('<div class="err-alert"><div>Incorrect name</div>');
        setTimeout(function() { $( ".err-alert" ).remove(); }, 5000);
      }
    }

    handlePasswordChange(){
      let password = $('#sign-up-password').val();
      if (password === '') {
        $(".err-alert").remove();
        $('#up-form').append('<div class="err-alert"><div>Incorrect password</div>');
        setTimeout(function() { $( ".err-alert" ).remove(); }, 5000);

      }
    }

    handlePasswordRepeatChange(){
      let password = $('#sign-up-password').val();
      let password_repeat = $('#sign-up-password_repeat').val();
      if (password !== password_repeat){
        $(".err-alert").remove();
        $('#up-form').append('<div class="err-alert"><div>Password and password repeat not equally </div>');
        setTimeout(function() { $( ".err-alert" ).remove(); }, 5000);
      }
    }

    handleLogoutClick(){
      cookie.remove('token', { path: '/' });
      this.state = {token: undefined};
      location.reload();
    }

    handleSignIn(event){
      let email = $('#sign-in-email').val();
      let password = $('#sign-in-password').val();
      let data = {'email' : email, 'password' : password};

      $.ajax({
        type: 'POST',
        url: LoginAPI,
        data: JSON.stringify(data),
        success : function(data) {
          console.log(data);
          cookie.save('token', data.token, { path: '/' });
          this.state = {token: data.token};
          location.reload();

        },
        error : function(data){
          console.log(data);
          $(".err-alert").remove();
          $('#in-form').append('<div class="err-alert">Incorrect username or password</div>');
          setTimeout(function() { $( ".err-alert" ).remove(); }, 5000);
        }
      });
      event.preventDefault();
    }

    handleSubmit(event) {
      let email = $('#sign-up-email').val();
      let name = $('#sign-up-name').val();
      let password = $('#sign-up-password').val();
      let password_repeat = $('#sign-up-password_repeat').val();
      let data = {'email' : email, 'name' : name, 'password' : password, 'password_repeat' : password_repeat};

      if (email === '' || /^([a-z0-9_\.-])+@[a-z0-9-]+\.([a-z]{2,4}\.)?[a-z]{2,4}$/i.test(email) === false){
        $(".err-alert").remove();
        $('#up-form').append('<div class="err-alert"><div>Incorrect email</div>');
        setTimeout(function() { $( ".err-alert" ).remove(); }, 5000);
      } else if (password !== password_repeat){
        $(".err-alert").remove();
        $('#up-form').append('<div class="err-alert"><div>Password and password repeat not equally </div>');
        setTimeout(function() { $( ".err-alert" ).remove(); }, 5000);
      } else if (name === ''){
        $(".err-alert").remove();
        $('#up-form').append('<div class="err-alert"><div>Incorrect name</div>');
        setTimeout(function() { $( ".err-alert" ).remove(); }, 5000);
      } else if (password === '') {
        $(".err-alert").remove();
        $('#up-form').append('<div class="err-alert"><div>Incorrect password</div>');
        setTimeout(function() { $( ".err-alert" ).remove(); }, 5000);
      } else if (password_repeat === ''){
        $(".err-alert").remove();
        $('#up-form').append('<div class="err-alert"><div>Incorrect password repeat</div>');
        setTimeout(function() { $( ".err-alert" ).remove(); }, 5000);
      } else {
        fetch(RegisterAPI, {
          method: 'post',
          headers: {

          },
          body: JSON.stringify(data)
        })
        .then(function (data) {
          console.log('Request succeeded with JSON response', data);
        })
        .catch(function (error) {
          console.log(error);
           $('#up-form').append('<div class="err-alert"><div>Email already use</div>');
           setTimeout(function() { $( ".err-alert" ).remove(); }, 5000);
        });
      }
      event.preventDefault();
    }

    render() {
        const isLoggedIn = this.state.token;

        let renderLogin = null;
        if(isLoggedIn !== undefined) {
          renderLogin = <UserGreeting onClick={this.handleLogoutClick} />;
        }
        if(isLoggedIn === undefined){
          renderLogin = <GuestGreeting
            handleSignIn={this.handleSignIn}
            handleSubmit={this.handleSubmit}
            handleMailChange={this.handleMailChange}
            handleNameChange={this.handleNameChange}
            handlePasswordChange={this.handlePasswordChange}
            handlePasswordRepeatChange={this.handlePasswordRepeatChange} />;
        }
        return (
          <div>
            {renderLogin}
          </div>
        )
    }
}

function UserGreeting(props) {
    return(
      <div>
        <h1 className="welcome-name">Welcome</h1>
        <Link className="btn main-btn" to="/organization">Organizations</Link><br/><br/>
        <Link className="btn main-btn" to="/profile">My Profile</Link><br/><br/>
        <button onClick={props.onClick}>
        Logout
      </button>
      </div>
    )
}

function GuestGreeting(props){
    return(
      <div className="App">
        <p className="App-intro">
        </p>
        <Grid>
          <Row className="show-grid">
            <Col xs={12} md={6}>
              <aside className="right-form">
                <h3 className="welcome-name">Authorization</h3>
                <form id="in-form" onSubmit={props.handleSignIn}>
                  <input className="btn form-input" id="sign-in-email" type="text" placeholder="Your email address" />
                  <input className="btn form-input" id="sign-in-password" type="password" placeholder="Your password" />
                  <input className="btn main-btn" type="submit" value="Sign in"/>
                </form>
              </aside>
            </Col>
            <Col xs={12} md={6}>
              <aside className="left-form">
                <h3 className="welcome-name">Registration</h3>
                <form id="up-form" onSubmit={props.handleSubmit}>
                  <input onBlur={props.handleMailChange} className="btn form-input" id="sign-up-email" type="text" placeholder="Your email address" />
                  <input onBlur={props.handleNameChange} className="btn form-input" id="sign-up-name" type="text" placeholder="Your name" />
                  <input onBlur={props.handlePasswordChange} className="btn form-input" id="sign-up-password" type="password" placeholder="Create a password" />
                  <input onBlur={props.handlePasswordRepeatChange} className="btn form-input" id="sign-up-password_repeat" type="password" placeholder="Password repeat" />
                  <input className="btn main-btn" type="submit" value="Sign up"/>
                  <Link to="forgot_password">Forgot password?</Link>
                </form>
              </aside>
            </Col>
          </Row>
        </Grid>
      </div>
    );
}

class SignUp extends Component {
  render() {
    return (
     <LoginControl />
    );
  }
}

export default SignUp;
