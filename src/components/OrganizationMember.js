/**
 * Created by user on 25.01.2017.
 */
import React, { Component } from 'react';
import '../css/Profile.css';
import { Link } from 'react-router';
import cookie from 'react-cookie';
import $ from 'jquery';


//Temp Api
const DETAIL_MEMBER = 'https://coffeecubes.ru/api/v1/organizations/detail_member/?token='+cookie.load('token');

class OrganizationMember extends Component {
  constructor(props){
    super(props);
    this.componentDidMount = this.componentDidMount.bind(this);
    this.state = {memberDetail: null};
  }

  componentDidMount = ()=>{
    let organization_user_id = parseInt(this.props.params.member_id, 10);
    let data = {"organization_user_id" : organization_user_id};

    $.ajax({
      type: 'POST',
      url: DETAIL_MEMBER,
      data: JSON.stringify(data),
      processData: false,
      crossDomain: true,
      success: function(result) {
        this.setState({memberDetail : result});
      }.bind(this),
      error: function(xhr, status, err) {
        console.error(DETAIL_MEMBER, status, err.toString());
      }
    });

  };

  render() {
    let memberName = null;
    let memberEmail = null;
    if(this.state.memberDetail){
      memberName = this.state.memberDetail.user.name;
      memberEmail = this.state.memberDetail.user.email
    }

    return (
      <div className="valid-component">
        <MemberInfo memberUserName={memberName} memberUserEmail={memberEmail}/>
        <Link to="/" className="">Back</Link>
      </div>
    );
  }
}


function MemberInfo(props){
  return(
    <div>
      <h1>{props.memberUserName}</h1>
      <h3>{props.memberUserEmail}</h3>
    </div>
  );
}

export default OrganizationMember;