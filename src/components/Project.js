/**
 * Created by user on 20.02.2017.
 */
import React, { Component } from 'react';
import cookie from 'react-cookie';
import {Link, hashHistory}  from 'react-router';

import Grid from 'react-bootstrap/lib/Grid';
import Row from 'react-bootstrap/lib/Row';
import Col from 'react-bootstrap/lib/Col';
import Jumbotron from 'react-bootstrap/lib/Jumbotron';
import Accordion from 'react-bootstrap/lib/Accordion';
import Panel from 'react-bootstrap/lib/Panel';
import Table from 'react-bootstrap/lib/Table';
import Tab from 'react-bootstrap/lib/Tab';
import Tabs from 'react-bootstrap/lib/Tabs';

import Kronos from 'react-kronos'

import '../css/Project.css';

const TEAM_PROJECT_LIST = 'https://coffeecubes.ru/api/v1/projects/list_team/?token='+cookie.load('token');
const PROJECT_DETAIL = 'https://coffeecubes.ru/api/v1/projects/detail/?token='+cookie.load('token');
const PROJECT_UPDATE = 'https://coffeecubes.ru/api/v1/projects/update/?token='+cookie.load('token');

const CONTRACT_CREATE = 'https://coffeecubes.ru/api/v1/contracts/create/?token='+cookie.load('token');
const CONTRACT_LIST = 'https://coffeecubes.ru/api/v1/contracts/list_project/?token='+cookie.load('token');
const CONTRACT_DETAIL = 'https://coffeecubes.ru/api/v1/contracts/detail/?token='+cookie.load('token');
const CONTRACT_UPDATE = 'https://coffeecubes.ru/api/v1/contracts/update/?token='+cookie.load('token');
const CONTRACT_COMPLETE = 'https://coffeecubes.ru/api/v1/contracts/complete/?token='+cookie.load('token');
const CONTRACT_DELETE = 'https://coffeecubes.ru/api/v1/contracts/delete/?token='+cookie.load('token');

const TASK_CREATE = 'https://coffeecubes.ru/api/v1/tasks/create/?token='+cookie.load('token');
const TASK_LIST = 'https://coffeecubes.ru/api/v1/tasks/list_contract/?token='+cookie.load('token');
const TASK_UPDATE = 'https://coffeecubes.ru/api/v1/tasks/update/?token='+cookie.load('token');
const TASK_DELETE = 'https://coffeecubes.ru/api/v1/tasks/delete/?token='+cookie.load('token');

class RequestToApi {
  constructor(url, data) {
    this.url = url;
    this.data = data;
  }
  request() {
    fetch(this.url, {
      method: 'POST',
      mode: 'cors',
      body:JSON.stringify(this.data)
    })
    .then(function(response) {
      // console.log(response.headers.get('Content-Type'));
      //console.log(response.status);
      return response.json();
    })
    .then(function(json) {
      console.log(json);
      let result = json;
      return result;
    })
    .catch( console.log );
  }
}

class Project extends Component {
  constructor(props) {
    super(props);
    this.handleUpdateProject = this.handleUpdateProject.bind(this);
    this.handleUpdateProjectComment = this.handleUpdateProjectComment.bind(this);
    this.handleProjectChange = this.handleProjectChange.bind(this);
    this.handleCreateContract = this.handleCreateContract.bind(this);
    this.handleUpdateContract = this.handleUpdateContract.bind(this);
    this.handleDeleteContract = this.handleDeleteContract.bind(this);
    this.handleDetailContract = this.handleDetailContract.bind(this);
    this.handleCreateTask = this.handleCreateTask.bind(this);
    this.handleDeleteTask = this.handleDeleteTask.bind(this);
    this.handleUpdateTask = this.handleUpdateTask.bind(this);
    this.handlePreUpdateTask = this.handlePreUpdateTask.bind(this);
    this.handleCancelUpdateTask = this.handleCancelUpdateTask.bind(this);

    this.state = {
      projectList : null,
      projectDetail : null,
      currentProjectId : null,
      currentContractId : null,
      contractList : null,
      contractName : null,
      contractBudget : null,
      contractId : null,
      contractStart : null,
      contractFinish : null,
      taskList : null
    }
  }

  componentDidMount = () => {
    //Список проектов команды
    let team_id = this.props.params.team_id; // Получаем данные о текущем переходе по идентификаору команды
    team_id = parseInt(team_id, 10);
    let data = {"team_id": team_id};
    let _this = this;

    fetch(TEAM_PROJECT_LIST, {
      method: 'POST',
      mode: 'cors',
      body: JSON.stringify(data)
    })
      .then(function (response) {
        return response.json();
      })
      .then(function (result) {
        //console.log(result);
        _this.setState({projectList: result});
      })
      .catch(console.log);

    //Подробности текущего проекта
    let id = parseInt(this.props.params.project_id, 10);
    let dataProject = {"id": id};
    fetch(PROJECT_DETAIL, {
      method: 'POST',
      mode: 'cors',
      body: JSON.stringify(dataProject)
    })
      .then(function (response) {
        return response.json();
      })
      .then(function (result) {
        //console.log(result);
        _this.setState({projectDetail: result});
        _this.setState({currentProjectId: result.id});
      })
      .catch(console.log);


    //Выводим список текущих контрактов для текущего проекта.
    let projectId = parseInt(this.props.params.project_id, 10);
    let contractsProject = {"project_id": projectId};
    fetch(CONTRACT_LIST, {
      method: 'POST',
      mode: 'cors',
      body: JSON.stringify(contractsProject)
    })
      .then(function (response) {
        return response.json();
      })
      .then(function (result) {
        _this.setState({contractList: result});
        _this.setState({currentContractId: result});
        //console.log(result);
      })
      .catch(console.log);
  };

  handleUpdateProject(event){
    //let id = parseInt(this.props.params.project_id, 10);
    //let data = {"id" : id};

    console.log(event.target);
  }

  handleProjectChange(){
    let _this = this;
    setTimeout(function() {
      _this.setState({currentProjectId : _this.props.params.project_id});
      let id = parseInt(_this.props.params.project_id, 10);
      let dataProject = {"id": id};
      fetch(PROJECT_DETAIL, {
        method: 'POST',
        mode: 'cors',
        body: JSON.stringify(dataProject)
      })
        .then(function (response) {
          return response.json();
        })
        .then(function (result) {
          //console.log(result);
          _this.setState({projectDetail: result});
          _this.setState({currentProjectId: result.id});
          document.querySelector('.text-comment').value = result.comment;
        })
        .catch(console.log);
      //Выводим список текущих контрактов для текущего проекта.
      let projectId = parseInt(_this.props.params.project_id, 10);
      let contractsProject = {"project_id": projectId};
      fetch(CONTRACT_LIST, {
        method: 'POST',
        mode: 'cors',
        body: JSON.stringify(contractsProject)
      })
        .then(function (response) {
          return response.json();
        })
        .then(function (result) {
          _this.setState({contractList: result});
          _this.setState({contractName : null, contractBudget : null, contractId : null});
          _this.setState({contractStart : null, contractFinish : null, taskList : null});
          //console.log(result);
        })
        .catch(console.log);

    }, 4);
  }

  handleUpdateProjectComment(event){
    let id = parseInt(this.props.params.project_id, 10);
    let name = this.state.projectDetail.name;

    let data = {"id" : id, "name": name, "start_date": "", "comment" : event.target.value};
    let RequestUpdateProject = new RequestToApi(PROJECT_UPDATE, data);
    RequestUpdateProject.request();
  }

  handleCreateContract(event){
    let id = parseInt(this.props.params.project_id, 10);
    let data = {"project_id" : id, "name": event.target.querySelector('#add-contract').value};
    let RequestUpdateProject = new RequestToApi(CONTRACT_CREATE, data);
    RequestUpdateProject.request();
    event.preventDefault();
  }

  handleUpdateContract(event){
    console.log(event.target);
  }

  handleDeleteContract(event){
    let id = parseInt(event.target.name, 10);
    let data = {"contract_id" : id};
    let RequestUpdateProject = new RequestToApi(CONTRACT_DELETE, data);
    RequestUpdateProject.request();
  }
//
  handleDetailContract(event){
    if(event.target.parentNode.parentNode.parentNode.getAttribute('data-val')){
      let _this = this;
      console.log(event.target.parentNode.parentNode.parentNode.getAttribute('data-val'));
      let id = parseInt(event.target.parentNode.parentNode.parentNode.getAttribute('data-val'), 10);
      let dataContract = {"contract_id": id};
      fetch(CONTRACT_DETAIL, {
        method: 'POST',
        mode: 'cors',
        body: JSON.stringify(dataContract)
      })
        .then(function (response) {
          return response.json();
        })
        .then(function (result) {
          //console.log(result);
          _this.setState({contractName : result.name, contractBudget : result.budget, contractId : result.id});
          _this.setState({contractStart : result.start_date, contractFinish : result.finish_date});
        })
        .catch(console.log);

      //получаем список Тасков
      let contractTasks = {"contract_id": id};
      fetch(TASK_LIST, {
        method: 'POST',
        mode: 'cors',
        body: JSON.stringify(contractTasks)
      })
        .then(function (response) {
          return response.json();
        })
        .then(function (result) {
          _this.setState({taskList: result});
          console.log(result);
        })
        .catch(console.log);
    }
  }

  handleCreateTask(event){
    let _this = this;
    let id = parseInt(this.state.contractId, 10);
    let name = event.target.querySelector('#add-task').value;
    let contractTasks = {"contract_id": id, "name" : name};
    fetch(TASK_CREATE, {
      method: 'POST',
      mode: 'cors',
      body: JSON.stringify(contractTasks)
    })
      .then(function (response) {
       return response.json();
      })
      .then(function (result) {
        _this.setState({taskList: result});
        console.log(result);
      })
      .catch(console.log);
    event.preventDefault();

  }

  handlePreUpdateTask(event){
    let hiddenInputList = event.target.parentNode.querySelectorAll('.temp-hidden');
    console.log(hiddenInputList);
    for(let i = 0; i < hiddenInputList.length;i++){
      hiddenInputList[i].classList.remove('hidden');
    }

    let notHiddenInputList = event.target.parentNode.querySelectorAll('.temp-no-hidden');
    for(let i = 0; i < notHiddenInputList.length;i++){
      notHiddenInputList[i].classList.add('hidden');
    }
    event.target.parentNode.querySelector('#name'+event.target.name).value = event.target.parentNode.querySelector('.name').innerHTML;
    event.target.parentNode.querySelector('#assigment_to_'+event.target.name).value = event.target.parentNode.querySelector('.assigment_to').innerHTML;
    event.target.parentNode.querySelector('#bonus_percent'+event.target.name).value = event.target.parentNode.querySelector('.bonus_percent').innerHTML;
    event.target.parentNode.querySelector('#completed'+event.target.name).value = event.target.parentNode.querySelector('.completed').innerHTML;
    event.target.parentNode.querySelector('#deadline_date'+event.target.name).value = event.target.parentNode.querySelector('.deadline_date').innerHTML;
    event.target.parentNode.querySelector('#hour_amount'+event.target.name).value = event.target.parentNode.querySelector('.hour_amount').innerHTML;
    event.target.parentNode.querySelector('#hour_tax'+event.target.name).value = event.target.parentNode.querySelector('.hour_tax').innerHTML;
    event.target.parentNode.querySelector('#outlay'+event.target.name).value = event.target.parentNode.querySelector('.outlay').innerHTML;
    event.target.parentNode.querySelector('#sum'+event.target.name).value = event.target.parentNode.querySelector('.sum').innerHTML;
  }

  handleCancelUpdateTask(event){
    let hiddenInputList = event.target.parentNode.querySelectorAll('.temp-hidden');
    console.log(hiddenInputList);
    for(let i = 0; i < hiddenInputList.length;i++){
      hiddenInputList[i].classList.add('hidden');
    }
    let notHiddenInputList = event.target.parentNode.querySelectorAll('.temp-no-hidden');
    for(let i = 0; i < notHiddenInputList.length;i++){
      notHiddenInputList[i].classList.remove('hidden');
    }
  }

  handleUpdateTask(event){
    let hiddenInputList = event.target.parentNode.querySelectorAll('.temp-hidden');
    for(let i = 0; i < hiddenInputList.length;i++){
      hiddenInputList[i].classList.add('hidden');
    }
    let notHiddenInputList = event.target.parentNode.querySelectorAll('.temp-no-hidden');
    for(let i = 0; i < notHiddenInputList.length;i++){
      notHiddenInputList[i].classList.remove('hidden');
    }

    console.log(event.target.name);

    let taskId = parseInt(event.target.name, 10);
    let contractId = parseInt(this.state.contractId,10);
    let name = event.target.querySelector('#name'+event.target.name).value;
    let assigmentTo =  parseInt(event.target.parentNode.querySelector('#assigment_to_'+event.target.name).value,10);
    let bonusPercent =  parseInt(event.target.parentNode.querySelector('#bonus_percent'+event.target.name).value,10);
    let deadlineData = event.target.parentNode.querySelector('#deadline_date'+event.target.name).value;
    let hourTax = parseFloat(event.target.parentNode.querySelector('#hour_tax'+event.target.name).value);
    let hourAmount = parseFloat(event.target.parentNode.querySelector('#hour_amount'+event.target.name).value);
    let outlay = parseFloat(event.target.parentNode.querySelector('#outlay'+event.target.name).value);
    console.log(outlay);
    let data = {"id" : taskId,
      "contract_id" : contractId,
      "name" : name,
      "assignment_to" : assigmentTo,
      "deadline_date" : deadlineData,
      "hour_tax" : hourTax,
      "hour_amount" : hourAmount,
      "bonus_percent" : bonusPercent,
      "outlay" : false};

    let RequestUpdateTask = new RequestToApi(TASK_UPDATE, data);
    RequestUpdateTask.request();

    event.preventDefault();
  }

  handleDeleteTask(event){
    console.log(event.target);
    let taskId = parseInt(event.target.name, 10);
    let data = {"task_id" : taskId};
    let RequestUpdateTask = new RequestToApi(TASK_DELETE, data);
    RequestUpdateTask.request();
  }

  render() {
    return (
      <Grid className="project-container">
        <Row className="show-grid">
          <Col xs={12} md={4}>
            <DetailProject
              projectInfo={this.state.projectDetail}
              handleUpdateProjectComment={this.handleUpdateProjectComment}
            />
          </Col>
          <Col xs={12} md={8}>
            <ProjectFastView projects={this.state.projectList}/>
            <CreateContract handleCreateContract={this.handleCreateContract} />
            <ContractsList
              contracts={this.state.contractList}
              handleUpdateContract={this.handleUpdateContract}
              handleDeleteContract={this.handleDeleteContract}
              handleDetailContract={this.handleDetailContract}
              contractDetail={this.state.contractName}
              taskList={this.state.taskList}
              handleCreateTask={this.handleCreateTask}
              handleUpdateTask={this.handleUpdateTask}
              handlePreUpdateTask={this.handlePreUpdateTask}
              handleCancelUpdateTask={this.handleCancelUpdateTask}
              handleDeleteTask={this.handleDeleteTask}
            />

          </Col>
        </Row>
      </Grid>
    );
  }
}

function ProjectFastView(props){
 if(props.projects) {
   return (
      <Row className="show-grid">
        <Col md={4}>
          <Panel header="Hours">
            54
          </Panel>
        </Col>
        <Col md={4}>
          <Panel header="Net income">
            1k
          </Panel>
        </Col>
        <Col md={4}>
          <Panel header="Profit">
            23%
          </Panel>
        </Col>
      </Row>
   )
 } else {
   return(
     <div />
   )
 }
}
//a ss ad nnn [das
function ContractsList(props){
 if(props.contracts) {
    return (
      <div>
        <h3>Contracts</h3>
          <Accordion>
            {props.contracts.contracts.map((item, i) =>
              <Panel onClick={props.handleDetailContract} data-val={item.id} header={item.name} key={i}  eventKey={i}>
                Date:
                <input type="text" className="" name={item.start_date} value={item.start_date}/>
                <input type="text" className="" name={item.finish_date} value={item.finish_date}/>
                Budget:
                <input type="text" className="" name={item.budget} value={item.budget}/>
                <input type="submit" name={item.id} className="btn" onClick={props.handleDeleteContract} value="X"/>

                <Tabs defaultActiveKey={1} id="uncontrolled-tab-example">
                  <Tab eventKey={1} title="Tasks">
                    <h4 className="contract-sub-header">Payments</h4>
                    <Table responsive>
                      <thead>
                        <tr>
                          <th></th>
                          <th>Title</th>
                          <th>Note</th>
                          <th>Responsible</th>
                          <th>Date</th>
                          <th>Summ</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td><input type="submit" className="btn" name={item.id} value="X"/></td>
                          <td>Table cell</td>
                          <td><input type="text" value="Befor start"/></td>
                          <td><select><option>Jon Smith</option></select></td>
                          <td><input type="text" value="10.05.2017"/></td>
                          <td><input type="text" value="$ 1 000"/></td>
                        </tr>
                        <tr>
                          <td><input type="submit" className="btn" name={item.id} value="X"/></td>
                          <td>Table cell</td>
                          <td><input type="text" value="After Design"/></td>
                          <td><select><option>Jon Smith</option></select></td>
                          <td><input type="text" value="10.05.2017"/></td>
                          <td><input type="text" value="$ 1 000"/></td>
                        </tr>
                      </tbody>
                    </Table>
                    <h4 className="contract-sub-header">Expences</h4>
                    <Table responsive>
                      <thead>
                        <tr>
                          <th></th>
                          <th>Title</th>
                          <th>Note</th>
                          <th>Responsible</th>
                          <th>Date</th>
                          <th>Summ</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td><input type="submit" className="btn" name={item.id} value="X"/></td>
                          <td>Table cell</td>
                          <td><input type="text" value="After Design"/></td>
                          <td><select><option>Jon Smith</option></select></td>
                          <td><input type="text" value="10.05.2017"/></td>
                          <td><input type="text" value="$ 1 000"/></td>
                        </tr>
                      </tbody>
                    </Table>
                    <h4 className="contract-sub-header">Tasks</h4>
                    <Table responsive>
                      <thead>
                      <tr>
                        <th></th>
                        <th>Name</th>
                        <th>Assigment to</th>
                        <th>Bonus percent</th>
                        <th>Deadline</th>
                        <th>Hour amount</th>
                        <th>Hour tax</th>
                        <th>Outlay</th>
                        <th>Sum</th>
                      </tr>
                      </thead>
                      <tbody>
                      {props.taskList ? props.taskList.tasks.map((item, i) =>
                        <tr key={i}>
                          <td>
                            <input type="submit" className="btn" onClick={props.handleDeleteTask} name={item.id} value="X"/>
                          </td>
                          <td>
                            <input type="text" className="form-input btn" value={item.name} id={"name"+item.id}/><br/>
                          </td>
                          <td>
                            <input type="text" className="form-input btn" id={"assigment_to_"+item.id} value={item.assigment_to}/><br/>
                          </td>
                          <td>
                            <input type="text" className="form-input mini-form-input btn" value={item.bonus_percent} id={"bonus_percent"+item.id}/><br/>
                          </td>
                          <td>
                            <Kronos
                              date="ISO"
                            />
                          </td>
                          <td>
                            <input type="text" className="form-input mini-form-input btn" value={item.hour_amount} id={"hour_amount"+item.id}/><br/>
                          </td>
                          <td>
                            <input type="text" className="form-input mini-form-input btn" value={item.hour_tax} id={"hour_tax"+item.id}/><br/>
                          </td>
                          <td>
                            <select className="form-input btn mini-form-input" id={"outlay"+item.id}>
                              <option value="false">false</option>
                              <option value="true">true</option>
                            </select><br/>
                          </td>
                          <td>
                            <input type="text" value={item.sum} className="form-input mini-form-input btn" id={"sum"+item.id}/><br/>
                          </td>

                        </tr>
                      ): false}
                      </tbody>
                    </Table>
                      <form id="up-form" className="add-task-form" onSubmit={props.handleCreateTask}>
                        <Row className="show-grid">
                          <Col md={4}>
                            <input className="btn form-input" id="add-task" type="text" placeholder="Task Name"/>
                          </Col>
                          <Col md={2}>
                            <input className="btn main-btn" type="submit" value="add task"/>
                          </Col>
                        </Row>
                      </form>
                  </Tab>
                  <Tab eventKey={2} title="Team">
                    <Table responsive>
                      <thead>
                        <tr>
                          <th>Responsible</th>
                          <th>Tasks</th>
                          <th>Hour Budget</th>
                          <th>Hours Amount</th>
                          <th>Due Date</th>
                          <th>Complete Status</th>
                          <th>Billable amount</th>
                          <th>Cost</th>
                          <th>Bonus 20%</th>
                          <th>Tam Bonus</th>
                          <th>Summ</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>Table cell</td>
                          <td>Table cell</td>
                          <td>10</td>
                          <td>10</td>
                          <td>10.05.2017</td>
                          <td>12.05.2017</td>
                          <td>1000</td>
                          <td>500</td>
                          <td>0</td>
                          <td>50</td>
                          <td>550</td>
                        </tr>
                        <tr>
                          <td>Table cell</td>
                          <td>Table cell</td>
                          <td>10</td>
                          <td>10</td>
                          <td>10.05.2017</td>
                          <td>12.05.2017</td>
                          <td>1000</td>
                          <td>500</td>
                          <td>0</td>
                          <td>50</td>
                          <td>550</td>
                        </tr>
                        <tr>
                          <td>Table cell</td>
                          <td>Table cell</td>
                          <td>10</td>
                          <td>10</td>
                          <td>10.05.2017</td>
                          <td>12.05.2017</td>
                          <td>1000</td>
                          <td>500</td>
                          <td>0</td>
                          <td>50</td>
                          <td>550</td>
                        </tr>
                        <tr className="result-row-contract">
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                          <td>2 000</td>
                          <td>940</td>
                          <td>940</td>
                          <td>90</td>
                          <td className="result-col-contract">1070</td>
                        </tr>
                      </tbody>
                    </Table>
                  </Tab>
                  <Tab eventKey={3} title="Profit">
                    <Row className="show-grid">
                      <Col md={4}>
                        <Table responsive>
                          <thead>
                            <tr>
                              <th></th>
                              <th>&nbsp;</th>
                              <th></th>
                            </tr>
                            <tr>
                              <th>Tasks</th>
                              <th>Tasks Budget</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td>Table cell</td>
                              <td>550</td>
                            </tr>
                            <tr>
                              <td>Table cell</td>
                              <td>550</td>
                            </tr>
                            <tr>
                              <td>Table cell</td>
                              <td>550</td>
                            </tr>
                            <tr className="result-row-contract">
                              <td></td>
                              <td className="result-col-contract">1070</td>
                            </tr>
                          </tbody>
                        </Table>
                      </Col>
                      <Col md={4}>
                        <Table responsive>
                          <thead>
                            <tr>
                              <th></th>
                              <th className="result-row-contract">Plan</th>
                              <th></th>
                            </tr>
                            <tr>
                              <th>Costs</th>
                              <th>Office Exp</th>
                              <th>Profit</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td>650</td>
                              <td>550</td>
                              <td>33%</td>
                            </tr>
                            <tr>
                              <td>260</td>
                              <td>550</td>
                              <td>33%</td>
                            </tr>
                            <tr>
                              <td>260</td>
                              <td>550</td>
                              <td>33%</td>
                            </tr>
                            <tr className="result-row-contract">
                              <td className="result-col-contract">1244</td>
                              <td className="result-col-contract">1070</td>
                              <td className="result-col-contract">40.4%</td>
                            </tr>
                          </tbody>
                        </Table>
                      </Col>
                      <Col md={4}>
                        <Table responsive>
                          <thead>
                            <tr>
                              <th></th>
                              <th className="result-row-contract">Fact</th>
                              <th></th>
                            </tr>
                            <tr>
                              <th>Costs</th>
                              <th>Office Exp</th>
                              <th>Profit</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td>550</td>
                              <td>550</td>
                              <td>33%</td>
                            </tr>
                            <tr>
                              <td>260</td>
                              <td>550</td>
                              <td>33%</td>
                            </tr>
                            <tr>
                              <td>260</td>
                              <td>550</td>
                              <td>33%</td>
                            </tr>
                            <tr className="result-row-contract">
                              <td className="result-col-contract">1244</td>
                              <td className="result-col-contract">1070</td>
                              <td className="result-col-contract">40.4%</td>
                            </tr>
                          </tbody>
                        </Table>
                      </Col>
                    </Row>
                    <Table responsive>
                      <thead>
                        <tr>
                          <th>Expences</th>
                          <th>Date</th>
                          <th>Responcible</th>
                          <th>Cost</th>
                          <th>Quantity</th>
                          <th>Summ</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>Domain name</td>
                          <td>10.05.2017</td>
                          <td>John Smith</td>
                          <td>50</td>
                          <td>1</td>
                          <td>50</td>
                        </tr>
                        <tr className="result-row-contract">
                          <td className="result-col-contract"></td>
                          <td className="result-col-contract"></td>
                          <td className="result-col-contract"></td>
                          <td className="result-col-contract"></td>
                          <td className="result-col-contract"></td>
                          <td className="result-col-contract">50$</td>
                        </tr>
                      </tbody>
                    </Table>
                    <span className="bottom-line"></span>
                    <span className="result-profit-contract">$2000 - $1244 - $50 = $706 (35%)</span>
                    <span className="result-profit-contract">$2000 - $1244 - $50 = $706 (35%)</span>
                    <span className="bottom-line"></span>
                  </Tab>
                </Tabs>
              </Panel>
            )}
        </Accordion>
      </div>
    )
 } else{
   return(
     <div />
   )
 }
}

function CreateContract(props) {
  return (
    <div>
      <form id="up-form" className="add-contract-form" onSubmit={props.handleCreateContract}>
        <Row className="show-grid">
          <Col md={5}>
            <input className="btn form-input" id="add-contract" type="text" placeholder="Contract Name"/>
          </Col>
          <Col md={2}>
            <input className="btn main-btn" type="submit" value="add contract"/>
          </Col>
        </Row>
      </form>
    </div>
  )
}

function DetailProject(props){
  if(props.projectInfo){
    return(
      <div className="project-info-container">
        <div>
          <h3>{props.projectInfo.name}</h3>
          <span className="bottom-line"></span>
            <div>
              Dates
              <input type="text" className="project-date" id={"start_date_value"+props.projectInfo.id} value={props.projectInfo.start_date}/>
              <input type="text" className="project-date" id={"finish_date_value"+props.projectInfo.id} value={props.projectInfo.finish_date}/>
            </div>
            <div>
              Status
              <input type="text" className="project-date" value="data is null"/>
            </div>
            <div>
              Team
              <input type="text" className="project-date" id={"finish_date_value"+props.projectInfo.id} value={props.projectInfo.finish_date}/>
            </div>
            <span className="bottom-line"></span>
            <div>
              Customer
              <input type="text" className="project-date" value="data is null"/>
              <input type="submit" className="project-date btn main-btn" value="New"/>
            </div>
            <div>
              Manager <input type="text" className="project-date" value="data is null" />
            </div>
            <div>
              Sales <input type="text" className="project-date" value="data is null" />
            </div>
            <p>
            Notes <textarea onBlur={props.handleUpdateProjectComment} className="text-comment" defaultValue={props.projectInfo.comment}></textarea>
          </p>
            <span className="bottom-line"></span>
            <div>
              Budget <input type="text" className="project-date" id={"budget_value"+props.projectInfo.id} value={props.projectInfo.budget}/>
            </div>
            <span className="bottom-line"></span>
            <div>
              Permissions <input type="text" className="project-date" value="data is null" />
            </div>
        </div>
      </div>
    )
  }else{
    return(
      <Jumbotron>
        <h1>LOADING...</h1>
        <p>This is a simple hero unit, a simple jumbotron-style component for calling extra attention to featured content or information.</p>
      </Jumbotron>
    )
  }
}

export default Project;