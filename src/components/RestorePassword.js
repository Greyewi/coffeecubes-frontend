/**
 * Created by user on 20.01.2017.
 */
import React, { Component } from 'react';
import { Link, browserHistory } from 'react-router';
import $ from 'jquery';


const RESET_PASSWORD_EXIST = 'https://coffeecubes.ru/api/v1/login/reset_password_token_exist/';
const RESET_PASSWORD_APPROVE = 'https://coffeecubes.ru/api/v1/login/reset_password_approve/';

class RestorePassword extends Component {
  constructor(props) {
    super(props);
    this.handleResetPassword = this.handleResetPassword.bind(this);
    this.state = {isForgot: null};
  }

  handleResetPassword(){
    let password = $('#password').val();
    let repeat_password = $('#password_repeat').val();
    if(password === repeat_password){
      var path = window.location.href.split('/');
      let data = {"token" : path[4], "password" : password, "password_repeat" : repeat_password};
      $.ajax({
        type: 'POST',
        url: RESET_PASSWORD_APPROVE,
        xhrFields: {
          withCredentials: false
        },
        data: JSON.stringify(data),
        contentType: false,
        processData: false,
        crossDomain: true,
        headers: {
        },
        success: function(result) {
          console.log('Пароль переименовался');
        },
        error: function(xhr, status, err) {
          console.error(RESET_PASSWORD_APPROVE, status, err.toString());
        }
      });
    }
  }


  componentDidMount = ()=>{
    //http://coffeecubes.ru/restore_password/a1e5c413-29f0-4079-bb6a-e88cf034ebeb/
    var path = window.location.href.split('/');
    $.ajax({
      type: 'POST',
      url: RESET_PASSWORD_EXIST,
      data:  JSON.stringify({"token" : path[4]}),
      processData: false,
      crossDomain: true,
      success: function(result) {
        console.log(result);
        this.setState({isForgot : true});
      }.bind(this),
      error: function(xhr, status, err) {
        console.error(RESET_PASSWORD_EXIST, status, err.toString());
      }
    });
  };



  render() {
    const isForgotten = this.state.isForgot;

    let RenderReset = null;
    if(isForgotten !== undefined){
      RenderReset = <ResetPassword handleResetPassword={this.handleResetPassword}/>;
    } else {
      RenderReset = <RedirectTo404/>;
    }

    return (
      <div className="valid-component">
        {RenderReset}
        <Link to="/" className="">Back</Link>
      </div>
    );
  }
}

function RedirectTo404(props){
  return(
    <div>
      {browserHistory.push('*')}
    </div>
  )
}


function ResetPassword(props){
  return(
    <div>
      <input type="password" className="btn form-input" id="password" placeholder="enter password"/>
      <input type="password" className="btn form-input" id="password_repeat" placeholder="repeat password" />
      <br/>
      <input type="button" onClick={props.handleResetPassword} className="btn" value="Reset" />
    </div>
  )
}





export default RestorePassword;