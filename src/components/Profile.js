/**
 * Created by user on 14.12.2016.
 */
import React, { Component } from 'react';
import '../css/Profile.css';
import $ from 'jquery';
import { Link } from 'react-router';
import cookie from 'react-cookie';
import Grid from 'react-bootstrap/lib/Grid';
import Row from 'react-bootstrap/lib/Row';
import Col from 'react-bootstrap/lib/Col';

const RETURN_PROFILE = 'https://coffeecubes.ru/api/profile/detail/?token='+cookie.load("token");
const UPDATE_NAME = 'https://coffeecubes.ru/api/profile/update/?token='+cookie.load("token");

const profile = {
    id : '1',
    name : 'Petya',
    email : 'sms@ckdigital.ru',
    photo : 'http://modernhorrors.com/wp-content/uploads/2015/05/Hacker-Man-426x351.jpg',
    role : 'hackerMan',
    department : 'hackers',
    token : '1234567890',
    guest : '1'
};

class Profile extends Component {
  constructor(props) {
    super(props);
    this.handleUpdateName = this.handleUpdateName.bind(this);
    this.state = {name : null};
    this.state = {id : null};
    this.state = {email : null};
  }

  componentDidMount = ()=>{
      $.ajax({
        type: "GET",
        url: RETURN_PROFILE,
        xhrFields: {
          withCredentials: false
        },
        crossDomain: true,
        dataType: 'json',
        success: (result)=>{
          this.setState({name : result.name});
          this.setState({id : result.id});
          this.setState({email : result.email});
          //console.log(result);
        },
        error: function(xhr, status, err) {
          console.error(RETURN_PROFILE, status, err.toString());
        }
      });
    };

  handleUpdateName(){
    let data = {"name" : $('#currentName').val()};
    console.log(data);
    $.ajax({
      type: 'POST',
      url: UPDATE_NAME,
      xhrFields: {
        withCredentials: false
      },
      data: JSON.stringify(data),
      contentType: false,
      processData: false,
      crossDomain: true,
      headers: {
      },
      success: function(result) {
        console.log('Имя переименовалась');
        this.setState({ownerName : result.owner.name});
        this.setState({ownerEmail : result.owner.email});
      },
      error: function(xhr, status, err) {
        console.error(UPDATE_NAME, status, err.toString());
      }
    });
  }

  render() {
    return (
     <TeamComponent
       handleUpdateName={this.handleUpdateName}
       name={this.state.name}
       email={this.state.email}/>
    );
  }
}

function TeamComponent (props){
  return(
      <div className="team-component">
        <h2>Profile</h2>
          <Grid>
            <Row className="show-grid">
              <Col xs={12} md={6}>
                <div>
                  <img src={profile.photo} alt={props.name} className="img-thumbnail profile-photo" />
                </div>
              </Col>
              <Col xs={12} md={6}>
                <div>
                  <table className="table table-bordered profile-table">
                    <tbody>
                      <tr>
                        <th>Email</th><td>{props.email}</td>
                      </tr>
                      <tr>
                        <th>Name</th>
                        <td><input type="text" id="currentName" placeholder={props.name} /><br/><button onClick={props.handleUpdateName} className="btn">edit</button></td>
                      </tr>
                      <tr>
                        <th>Change password</th>
                        <td>
                          <Link to="profile/change_password" className="btn">Change Password</Link>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </Col>
            </Row>
          </Grid>
        <Link to="/" className="">Back</Link>
      </div>
    )
}

export default Profile;