/**
 * Created by user on 13.12.2016.
 */
import React, { Component } from 'react';
import '../css/Footer.css';


class Footer extends Component {
  render() {
    return (
      <div className="footer-container">
          <div className="copyright">© site-consulting</div>
      </div>
    );
  }
}

export default Footer;