/**
 * Created by user on 24.01.2017.
 */
import React, { Component } from 'react';
import '../css/Teams.css';
import { Link } from 'react-router';
import cookie from 'react-cookie';
import $ from 'jquery';
import Accordion from 'react-bootstrap/lib/Accordion';
import Panel from 'react-bootstrap/lib/Panel';
import Button from 'react-bootstrap/lib/Button';
import { browserHistory } from 'react-router';

//Team Detail

const DETAIL_ORGANIZATION = 'https://coffeecubes.ru/api/v1/organizations/detail/?token='+cookie.load('token');
const ADD_MEMBER = 'https://coffeecubes.ru/api/v1/organizations/add_member/?token='+cookie.load('token');
const LIST_TEAM = 'https://coffeecubes.ru/api/teams/list/?token='+cookie.load('token');
const ADD_TEAM = 'https://coffeecubes.ru/api/v1/teams/create/?token='+cookie.load('token');


// const DELETE_MEMBER = 'https://coffeecubes.ru/api/teams/delete_user?token='+cookie.load('token');
// const UPDATE_MEMBER = 'https://coffeecubes.ru/api/teams/update_user?token='+cookie.load('token');


class Teams extends Component {
  constructor(props){
    super(props);
    this.componentDidMount = this.componentDidMount.bind(this);
    this.handleAddClick = this.handleAddClick.bind(this);
    this.handleAddTeamClick = this.handleAddTeamClick.bind(this);
    this.state = {open: false, OrgDetail : null, teamsList : null};
  }

  componentDidMount = ()=>{
    //Получаем данные о организации
    let id = this.props.params.organization_id; // Получаем данные о текущем переходе по идентификаору организации
    id = parseInt(id, 10);
    let data = {"id" : id};
    //console.log(this.props.params.organization_id);

    $.ajax({
      type: 'POST',
      url: DETAIL_ORGANIZATION,
      xhrFields: {
        withCredentials: false
      },
      data: JSON.stringify(data),
      contentType: false,
      processData: false,
      crossDomain: true,
      headers: {
      },
      success: function(result) {
        //console.log(result);
        this.setState({OrgDetail : result});
      }.bind(this),
      error: function(xhr, status, err) {
        console.error(DETAIL_ORGANIZATION, status, err.toString());
        browserHistory.push('not_access');
      }
    });

    // Получаем список команд организации
    $.ajax({
      type: 'POST',
      url: LIST_TEAM,
      xhrFields: {
        withCredentials: false
      },
      contentType: false,
      processData: false,
      crossDomain: true,
      headers: {
      },
      success: function(result) {
        console.log(result);
        this.setState({teamsList : result});
      }.bind(this),
      error: function(xhr, status, err) {
        console.error(LIST_TEAM, status, err.toString());
      }
    });
  };

  handleAddClick(event){
    let id = this.props.params.organization_id; // Получаем данные о текущем переходе по идентификаору команды
    id = parseInt(id, 10);
    let userEmail = $('#add-member').val();
    let data = {"organization_id" : id, "email" : userEmail};

    $.ajax({
      type: 'POST',
      url: ADD_MEMBER,
      xhrFields: {
        withCredentials: false
      },
      data: JSON.stringify(data),
      contentType: false,
      processData: false,
      crossDomain: true,
      headers: {
      },
      success: function(result) {
        //console.log('Участник добавился');
        // this.setState({ownerName : result.owner.name});
      },
      error: function(xhr, status, err) {
        console.error(ADD_MEMBER, status, err.toString());
      }
    });
    event.preventDefault();
  }

  handleAddTeamClick(event){
    let id = this.props.params.organization_id; // Получаем данные о текущем переходе по идентификаору команды
    id = parseInt(id, 10);
    let teamName = $('#add-team').val();
    let data = {"name" : teamName, "organization_id" : id};

    $.ajax({
      type: 'POST',
      url: ADD_TEAM,
      xhrFields: {
        withCredentials: false
      },
      data: JSON.stringify(data),
      contentType: false,
      processData: false,
      crossDomain: true,
      headers: {
      },
      success: function(result) {
        console.log('Команда создалась');
        // this.setState({ownerName : result.owner.name});
      },
      error: function(xhr, status, err) {
        console.error(ADD_TEAM, status, err.toString());
      }
    });
    event.preventDefault();
  };

  render() {
    return (
      <div className="organization-detail-component">
        <OrganizationDetailInfo
          handleAddClick={this.handleAddClick}
          OrgDetail={this.state.OrgDetail}
          OrgId={this.props.params.organization_id}
        />
        <TeamList
          teamsList={this.state.teamsList}
          handleAddTeamClick={this.handleAddTeamClick}/>
        <TeamDetail teamC={this.props.children}/>
      </div>
    );
  }
}

function OrganizationDetailInfo(props) {
  if(props.OrgDetail) {
    return (
      <aside className="organization-detail-container">

        <h1>{props.OrgDetail.name}</h1>
        <ul className="list-align-left">
          <Link to="/organization/">Back</Link>
          <li>Creator - {props.OrgDetail.owner.name}</li>
          <li>Email - {props.OrgDetail.owner.email}</li>
        </ul>

      </aside>
    )
  } else {
    return (
      <span />
    )
  }
}

function TeamList(props) {
  if(props.teamsList) {
    return (
      <aside className="left-sidebar-container">
        <h3>Teams</h3>
          {props.teamsList.teams.map((item, i) =>
            <Panel header={item.name} key={i} eventKey={i} >
              <b>Name:</b>
              <span id={"hour_tax_maintain_value"+item.id}>{item.name}</span>
              <span className="memberId hidden">{item.id}</span>
              <Link className="btn main-btn" to={'/team-detail/' + item.id}>Detail</Link>
            </Panel>
          )}
        <h3 className="add-team-title">Add team</h3>
        <form id="up-form" onSubmit={props.handleAddTeamClick} >
          <input className="btn form-input" id="add-team" type="text" placeholder="Member name" />
          <input className="btn main-btn" type="submit" value="+"/>
        </form>
      </aside>
    )
  } else{
    return (
      <span />
    )
  }
}

function TeamDetail(props){
  return(
    <div id="team-detail-container">{props.teamC}</div>
  )
}

export default Teams;