/**
 * Created by user on 17.01.2017.
 */

import React, { Component } from 'react';
import '../css/Profile.css';
import { Link } from 'react-router';
import $ from 'jquery';


const RESET_PASSWORD = 'https://coffeecubes.ru/api/v1/login/reset_password_request/';

class ForgotPassword extends Component {
  constructor(props) {
    super(props);
    this.handleResetPassword = this.handleResetPassword.bind(this);
  }

  handleResetPassword(){
    let email = $('#email').val();
      let data = {"email" : email};
      $.ajax({
        type: 'POST',
        url: RESET_PASSWORD,
        xhrFields: {
          withCredentials: false
        },
        data: JSON.stringify(data),
        contentType: false,
        processData: false,
        crossDomain: true,
        headers: {
        },
        success: function(result) {
          console.log('Запрос отправлен');
        },
        error: function(xhr, status, err) {
          console.error(RESET_PASSWORD, status, err.toString());
        }
      });
  }





  render() {
    return (
      <div className="valid-component">
        <input type="email" className="btn form-input" id="email" placeholder="enter email"/>
        <input type="button" onClick={this.handleResetPassword} className="btn main-btn" value="reset"/>
        <br/>
        <Link to="/" className="">Back</Link>
      </div>
    );
  }
}

export default ForgotPassword;