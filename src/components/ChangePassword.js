/**
 * Created by user on 13.01.2017.
 */

import React, { Component } from 'react';
import '../css/ChangePassword.css';
import { Link } from 'react-router';
import cookie from 'react-cookie';
import $ from 'jquery';

const UPDATE_PASSWORD = 'https://coffeecubes.ru/api/profile/update_password/?token='+cookie.load("token");


class ChangePassword extends Component {
  constructor(props) {
    super(props);
    this.handleUpdatePassword = this.handleUpdatePassword.bind(this);
  }

  handleUpdatePassword(){
    let old_password = $('#old_password').val();
    let password = $('#password').val();
    let repeat_password = $('#password_repeat').val();
    if(password === repeat_password){
      let data = {"old_password" : old_password, "password" : password, "repeat_password" : repeat_password};
      $.ajax({
        type: 'POST',
        url: UPDATE_PASSWORD,
        xhrFields: {
          withCredentials: false
        },
        data: JSON.stringify(data),
        contentType: false,
        processData: false,
        crossDomain: true,
        headers: {
        },
        success: function(result) {
          console.log('Пароль переименовался');
        },
        error: function(xhr, status, err) {
          console.error(UPDATE_PASSWORD, status, err.toString());
        }
      });
    }

  }


  render() {

    return (
      <div className="valid-component">
        <UpdatePassword onClick={this.handleUpdatePassword} />
        <Link to="profile/" className="">Back</Link>
      </div>
    );
  }
}

function UpdatePassword(props){
  return(
    <div>
      <input type="password" className="btn form-input" id="old_password" placeholder="old password"/>
      <br/>
      <input type="password" className="btn form-input" id="password" placeholder="enter password"/>
      <input type="password" className="btn form-input" id="password_repeat" placeholder="repeat password" />
      <br/>
      <input type="button" onClick={props.handleUpdatePassword} className="btn" value="Change" />
    </div>
  )
}





export default ChangePassword;