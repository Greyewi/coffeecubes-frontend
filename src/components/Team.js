/**
 * Created by user on 14.12.2016.
 */
import React, { Component } from 'react';
import '../css/Team.css';
import { Link } from 'react-router';
import $ from 'jquery';
import cookie from 'react-cookie';
import Accordion from 'react-bootstrap/lib/Accordion';
import Panel from 'react-bootstrap/lib/Panel';

const CREATE_TEAM = 'https://coffeecubes.ru/api/teams/create?token='+cookie.load('token');
const RETURN_TEAM = 'https://coffeecubes.ru/api/teams/list?token='+cookie.load("token");
const UPDATE_TEAM = 'https://coffeecubes.ru/api/teams/update?token='+cookie.load("token");
const DELETE_TEAM = 'https://coffeecubes.ru/api/teams/delete?token='+cookie.load("token");


export class ListTeam extends Component {
  constructor(props) {
    super(props);
    this.handleCreateClick = this.handleCreateClick.bind(this);
    this.handleUpdateChange = this.handleUpdateChange.bind(this);
    this.handleUpdateKeyUp = this.handleUpdateKeyUp.bind(this);
    this.handleDeleteClick = this.handleDeleteClick.bind(this);
    this.autoPassName = this.autoPassName.bind(this);
    this.state = {result: null, ownerEmail: null, ownerName: null};
  }



    componentDidMount = ()=>{
      $.ajax({
        type: "GET",
        url: RETURN_TEAM,
        xhrFields: {
          withCredentials: false
        },
        crossDomain: true,
        dataType: 'json',
        success: (result)=>{
          //console.log(result.teams);
           this.setState({result : result.teams});
        },
        error: function(xhr, status, err) {
          console.error(RETURN_TEAM, status, err.toString());
        }
      });
    };

  handleDeleteClick(event){
    // TODO тут функция удаления!
  }

  handleCreateClick(event){
    let name = $('#create-team').val();
    let data = {'name' : name};

    $.ajax({
      type: 'POST',
      url: CREATE_TEAM,
      xhrFields: {
        withCredentials: false
      },
      data: JSON.stringify(data),
      contentType: false,
      processData: false,
      crossDomain: true,
      headers: {
      },
      success: function(result) {
        //console.log('Команда создалась');
        this.setState({ownerName : result.owner.name});
        this.setState({ownerEmail : result.owner.email});
      },
      error: function(xhr, status, err) {
        console.error(CREATE_TEAM, status, err.toString());

      }
    });
    event.preventDefault();
  }

  autoPassName(event){
    let text = event.target.placeholder;
    event.target.value = text;
  }

  handleUpdateChange(event){
    if((event.target.value !== "") && (event.target.value !== event.target.placeholder)){ // в placeholder храним изначальное имя
      //console.log(event.target.placeholder +'  '+event.target.value);
      let id = parseInt(event.target.parentNode.getElementsByTagName('span')[0].innerHTML, 10);
      let name = event.target.value;
      let data = {"id" : id, "name" : name};
      $.ajax({
        type: 'POST',
        url: UPDATE_TEAM,
        xhrFields: {
          withCredentials: false
        },
        data: JSON.stringify(data),
        contentType: false,
        processData: false,
        crossDomain: true,
        headers: {
        },
        success: function(result) {
          console.log('Команда переименовалась');
        },
        error: function(xhr, status, err) {
          console.error(CREATE_TEAM, status, err.toString());
        }
      });
    } else if(event.target.value === "") {
      console.log('событие удаления'+event);
      let id = parseInt(event.target.parentNode.getElementsByTagName('span')[0].innerHTML, 10);
      let name = event.target.placeholder;

      let data = {"id" : id, "name" : name};

      $.ajax({
        type: 'POST',
        url: DELETE_TEAM,
        xhrFields: {
          withCredentials: false
        },
        data: JSON.stringify(data),
        contentType: false,
        processData: false,
        crossDomain: true,
        headers: {
        },
        success: function(result) {
          console.log('Команда переименовалась');
          this.setState({ownerName : result.owner.name});
          this.setState({ownerEmail : result.owner.email});
        },
        error: function(xhr, status, err) {
          console.error(DELETE_TEAM, status, err.toString());
        }
      });
    }
  }

  handleUpdateKeyUp(event){
    let str = event.target.value;
    event.target.parentNode.parentNode.parentNode.parentNode.getElementsByTagName("a")[0].innerHTML = str;
    if(event.key === "backspace"){
      event.target.parentNode.parentNode.parentNode.parentNode.getElementsByTagName("a")[0].innerHTML.substring(0, str.length - 1)
    }
  }

  renderData(){
    if(this.state.result !== null){
      let thiss = this, i = 0;
        return(
          <div className="left-form">
            <h3>Teams</h3>
            <Accordion>
              {thiss.state.result.map(item =>
                <Panel header={item.name} eventKey={i++}>
                  <h4 className="team-title">
                    Команда:
                    <input placeholder={item.name} value={this.state.tempName} className="update-team-input" onFocus={this.autoPassName} onBlur={this.handleUpdateChange} onKeyUp={this.handleUpdateKeyUp} />
                    <div className="temp-team-name" >{item.name}</div>
                    <span className="hidden">{item.id}</span>
                  </h4>
                  <p>Создатель: {item.owner.name}<span className="opa-grey"> {item.owner.email} </span></p>
                  <Link className="btn main-btn" to={'/team-detail/' + item.id}>Detail</Link>
                </Panel>
              )}
            </Accordion>
          </div>
        );
    }
  }

  render() {
    let renderCreateTeam = null;
    //let renderUpdateTeam = null;

    renderCreateTeam = <CreateTeamRender handleCreateClick={this.handleCreateClick}/>;
    //renderUpdateTeam = <UpdateTeamRender />;

    return (
      <div>
        <div>{renderCreateTeam}</div>
        <div id="ListUl" className="team-list">{this.renderData()}</div>
      </div>
    );
  }
}

function CreateTeamRender(props){
  return(
     <div className="team-container">
       <h2>Team</h2>
       <aside className="right-form">
         <h3 className="create-team-title">Create Team</h3>
          <form id="up-form" onSubmit={props.handleCreateClick} >
            <input className="btn form-input" id="create-team" type="text" placeholder="Team name" />
            <input className="btn main-btn" type="submit" value="Create Team"/>
          </form>
         <Link to="/">Back</Link>

        </aside>
      </div>
    )
}


class Team extends Component {
  render() {
    return (
      <div className="team-component">
        <ListTeam/>
      </div>
    );
  }
}

export default Team;