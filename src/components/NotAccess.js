/**
 * Created by user on 13.01.2017.
 */
import React, { Component } from 'react';
import { Link } from 'react-router';

class NotAccess extends Component {
  render() {
    return (
      <div>
        <h1>No, really no! - 403</h1>
        <img src="https://static.atraxion.com/noaccess.jpg" alt="notAccess" /><br/>
        <Link to="/" className="">Back</Link>
      </div>
    );
  }
}

export default NotAccess;
