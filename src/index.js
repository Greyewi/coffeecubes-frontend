import React from 'react';
import ReactDOM from 'react-dom';

import SignUp from './components/SignUp';
import Header from './components/Header';
import Footer from './components/Footer';
import Profile from './components/Profile';
import Team from './components/Team';
import Validation from './components/Validation';
import NotFound from './components/NotFound';
import NotAccess from './components/NotAccess';
import AllProjects from './components/AllProjects';
import ChangePassword from './components/ChangePassword';
import ForgotPassword from './components/ForgotPassword';
import ConfirmEmail from './components/ConfirmEmail';
import RestorePassword from './components/RestorePassword';
import ConfirmInvite from './components/ConfirmInvite';
import Organization from './components/Organization';
import Teams from './components/Teams';
import OrganizationMember from './components/OrganizationMember';
import Project from './components/Project';

import { Router, Route, browserHistory } from 'react-router';
import './css/index.css';

ReactDOM.render(
  <Header />,
  document.getElementById('header')
);

ReactDOM.render(
  <Router history={browserHistory}>
    <Route path="/" component={SignUp} />

    <Route path="profile" component={Profile} />
    <Route path="profile/change_password" component={ChangePassword} />
    <Route path="register_approve" component={Validation} />
    <Route path="forgot_password" component={ForgotPassword} />
    <Route path="confirm_email" component={ConfirmEmail} />
    <Route path="restore_password/:token" component={RestorePassword} />
    <Route path="confirm_invite" component={ConfirmInvite} />

    <Route path="organization" component={Organization} />
    <Route path="organization/teams/:organization_id" component={Teams} />

    <Route path="organization/organization-detail/:organization_id/:member_id" component={OrganizationMember} />
    <Route path="team" component={Team} />
    <Route path="team-detail/:team_id" component={AllProjects} />
    <Route path="project/:team_id/:project_id" component={Project} />

    <Route path="*" component={NotFound} />
    <Route path="not_access" component={NotAccess} />
  </Router>,
  document.getElementById('root')
);

//forgot_password - Форма отправки email, для восстановления пароля
//confirm_email - станица проверки подтверждения почты
//restore_password - страница проверки запроса на восстановление пароля
//confirm_invite - страница подтверждения проверки инва

ReactDOM.render(
  <Footer />,
  document.getElementById('footer')
);
